'use strict';


function showArr(list) {

    function addArr(tag, arr) {
        let ul = document.createElement('ul');
        let li;

        tag.appendChild(ul);

        arr.forEach(function (item) {
            if (Array.isArray(item)) {
                addArr(li, item);
                return;
            }

            li = document.createElement('li');
            li.appendChild(document.createTextNode(item));
            ul.appendChild(li);
        });
    }

    let div = document.getElementById('place');

    addArr(div, list);

    let i = 3;
    let timerId = setInterval(function () {
        document.getElementById("timer").innerText = i;
        if (i === 0) {
            document.getElementById('place').classList.add("none");
            document.getElementById('timer').classList.add("none");
            clearInterval(timerId);
            list = [];
        }

        i--;
    }, 1000);

}

showArr(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);