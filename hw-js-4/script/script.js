"use strict";

function createNewUser() {
    let user ={};
    user.firstName = prompt("What is your name");
    user.lastName = prompt("What is your surname");

    user.getLogin = function () {
        return this.firstName.toString().toLowerCase().charAt(0) + this.lastName.toLowerCase();
    };

    return user;
}
let user = createNewUser();
console.log(`User login is: ${user.getLogin()}`);