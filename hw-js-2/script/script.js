"use strict";

let enteredNumber = prompt("Text your number");
while (!enteredNumber || enteredNumber.trim() === "" || isNaN(+enteredNumber) || typeof +enteredNumber !== "number"
|| !Number.isInteger(+enteredNumber)) {
    enteredNumber = prompt("Text your number");
}
if (enteredNumber > 5 || enteredNumber === 5) {
    for (let i = 0; i <= enteredNumber; i += 5) {
        if (i % 5 === 0) {
            console.log(i);
        } else {
            console.log(`Sorry, no numbers`);
        }
    }
}else if (enteredNumber < -5 || enteredNumber === -5){
    for (let i = -5; i >= enteredNumber; i -= 5) {
        if (i % 5 === 0) {
            console.log(i);
        } else {
            console.log(`Sorry, no numbers`);
        }
    }
}else {
    console.log(`Sorry, no numbers`);
}
