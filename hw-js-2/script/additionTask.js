"use strict";

let m = prompt("Enter your first number");
let n = prompt("Enter your second number");

while (!m || typeof +m !== "number" || isNaN(+m) || m.trim() === "" || !n || typeof +n !== "number" || isNaN(+n) || n.trim() === "") {
    m = prompt("Enter your first number");
    n = prompt("Enter your second number");
}
if (m < n) {
    let startVar = m;
    let temp = n;
    showPrimes(startVar, temp);
} else {
    let startVar = n;
    let temp = m;
    showPrimes(startVar, temp)
}

function showPrimes(startVar, temp) {
    for (let i = startVar; i < temp; i++) {
        let count = 0;
        for (let j = 1; j < i && count < 2; j++) {
            if (i % j === 0) {
                count++;
            }
        }
        if (count < 2) {
            console.log(i);
        }
    }
}


