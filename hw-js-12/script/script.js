"use strict";


let arrPicture = ["./img/1.jpg", "./img/2.jpg", "./img/3.JPG", "./img/4.png"];
let counter;
let pictureSlider;
let i = -1;

timeCount();
changePicture();
counter = setInterval(timeCount, 4000);
pictureSlider = setInterval(changePicture, 4000);

function changePicture() {
    i = (i === arrPicture.length - 1) ? 0 : (i + 1);
    document.getElementById("image-to-show").src = arrPicture[i];
}

function timeCount() {
    let seconds = 3;
    const timerId = setInterval(function () {
        let timer = document.getElementById("timer");
        timer.innerText = seconds;
        seconds -= 1;
        if (seconds <= 0) {
            clearInterval(timerId);
        }
    }, 1000);
}

let pauseBtn = document.getElementById("pause");
pauseBtn.addEventListener("click", onPauseBtnClick);

function onPauseBtnClick() {
    clearInterval(counter);
    clearInterval(pictureSlider);
}
let resumeBtn = document.getElementById("resume");
resumeBtn.addEventListener("click", onResumeBtnClick);

function onResumeBtnClick() {
    counter = setInterval(timeCount, 4000);
    pictureSlider = setInterval(changePicture, 4000);
}