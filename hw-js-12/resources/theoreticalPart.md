#Теоретический вопрос

1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
The setTimeout () method calls the function only once. The setInterval () method calls the function all the time until
it is stopped.
2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
The setTimeout () method with a zero delay will run after all the functions in the file have been executed.
3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?
All functions included in the setInterval () method cannot be clearence garbage collected, even if they are not called by reference.