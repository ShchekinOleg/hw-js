"use strict";

const buttons = document.querySelectorAll("button");
window.onkeydown = btnColor;

function btnColor(event) {
    buttons.forEach(function (el) {
        el.classList.remove("blue");
    });
    switch (event.key) {
        case 'Enter':
            enter.classList.add('blue');
            break;
        case 's':
            s.classList.add('blue');
            break;
        case 'e':
            e.classList.add('blue');
            break;
        case 'o':
            o.classList.add('blue');
            break;
        case 'n':
            n.classList.add('blue');
            break;
        case 'l':
            l.classList.add('blue');
            break;
        case 'z':
            z.classList.add('blue');
            break;
    }
}
