"use strict";


let firstNumber = prompt("Enter first number");
while (!firstNumber || typeof +firstNumber !== "number" || isNaN(+firstNumber) || firstNumber.trim() === "") {
    firstNumber = prompt("Text correct first number", firstNumber);
}
let secondNumber = prompt("Enter second number");
while (!secondNumber || typeof +secondNumber !== "number" || isNaN(+secondNumber) || secondNumber.trim() === "") {
    secondNumber = prompt("Text correct second number", secondNumber);
}
let mathOperation = prompt("Choose the operation. Text `+` or `-` or `*` or `/` ");


function calculator(a, b, oper) {
    switch (oper) {
        case "-":
            return (subtraction(a, b));
        case "+":
            return addition(a, b);
        case "/":
            return division(a, b);
        case "*":
            return multiplication(a, b);
        default:
            console.log("Wrong symbol");
            break;
    }
}

function addition(a, b) {
    return (+a + +b);
}

function multiplication(a, b) {
    return (a * b);
}

function subtraction(a, b) {
    return (a - b);
}

function division(a, b) {
    return (b === 0) ? (a / b) : (console.log("Cannot be divisible by 0, choose another number"));
}

console.log(calculator(firstNumber, secondNumber, mathOperation));