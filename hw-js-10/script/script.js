"use strict";

let firstInput = document.getElementById("firstInput");
let firstPassword = document.getElementById("firstPassword");
firstInput.addEventListener("mousedown", function(){ replaceDown (firstInput, firstPassword)});
firstInput.addEventListener("mouseup", function (){replaceUp(firstInput, firstPassword)});

let secondInput = document.getElementById("secondInput");
let secondPassword = document.getElementById("secondPassword");
secondInput.addEventListener("mousedown", function(){ replaceDown (secondInput, secondPassword)});
secondInput.addEventListener("mouseup", function (){replaceUp(secondInput, secondPassword)});

function replaceUp(cl, inp) {
    cl.classList.replace('fa-eye-slash', 'fa-eye');
    inp.type = "password";
}
function replaceDown(cl, inp) {
    cl.classList.replace('fa-eye', 'fa-eye-slash');
    inp.type = "text";
}

const button = document.getElementById("btn");
let errMessage = document.getElementById("errorMessage");
button.addEventListener("click", onBtnClick);
function onBtnClick() {
    if (firstPassword.value === secondPassword.value) {
        errMessage.innerText = "";
        alert("You are wellcome");
    } else {
        errMessage.innerText = "Нужно ввести одинаковые значения";
    }
}