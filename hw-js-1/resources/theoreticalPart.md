## Теоретический вопрос

1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
    Variable 'var' hasn`t block constraints, that is why you can 'var' is a global variable. 
    This type of variable is deprecated, you shouldn't use it any more.
    
    'let' and 'const' are variables that can only be used in the block in which they were created.
    'const' variable can`t be declared more then one times.
    
    
2. Почему объявлять переменную через var считается плохим тоном?
    'var' is a global variable. Declaring another variable 'var' with the same name inside any block, 
    such as a loop or a function, overwrites the value of the first 'var' variable with the same name. 