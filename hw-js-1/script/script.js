"use strict";

let name = prompt("Enter your name");
while (!name || name.trim() === "") {
    name = prompt("Text correct name", name);
}
let age = prompt("Text your age");
while (!age || typeof +age !== "number" || isNaN(+age) || age.trim() === "") {
    age = prompt("Text correct age value", age);
}
if (age < 18) {
    alert(`You are not allowed to visit this website`);
} else if (age >= 18 & age <= 22) {
    alert(confirm(`Are you sure you want to continue?`) ? `Welcome, ${name}` : `You are not allowed to visit this website`);
} else {
    alert(`Welcome, ${name}`);
}