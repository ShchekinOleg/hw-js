"use strict";

document.getElementById("price").onblur = function () {
    let priceConvertToDigit = +price.value;
    if (priceConvertToDigit < 0){
        document.getElementById("price").classList.add("onBlurLine");
        document.getElementById("negativeValueAlert").innerText = `Please enter correct price`;
    } else{
        document.getElementById("price").classList.remove("onBlurLine");
        document.getElementById("negativeValueAlert").innerText = "";
        let spanNode = document.createElement("span");
        spanNode.setAttribute('onclick', `removeSpanNode(this);`);
        let textNode = document.createTextNode(`Текущая цена: ${priceConvertToDigit}`);
        spanNode.appendChild(textNode);
        let textSpan = document.getElementById("spanText");
        textSpan.appendChild(spanNode);
    }
};
let removeSpannode = function(node) {
    node.parentNode.removeChild(node);
};