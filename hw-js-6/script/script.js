"use strict";

//1
const filterBy = function (arr, type) {
    let res = [];
    for (let iterator = 0; iterator < arr.length; iterator++) {
        if (typeof arr[iterator] !== type) {
            res.push(arr[iterator]);
        }
    }
    return res;
};
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));

//2
const filterBy1 = (arr, type) => arr.filter(item => typeof item !== type);
console.log(filterBy1(['hello', 'world', 23, '23', null], 'number'));
