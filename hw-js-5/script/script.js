"use strict";

function createNewUser() {
    let user = {};

    user.firstName = prompt("What is your name");
    user.lastName = prompt("What is your surname");
    user.birthday = prompt("Enter your birthday like: dd.mm.yyyy");

    user.getLogin = function () {
        return this.firstName.toString().toLowerCase().charAt(0) + this.lastName.toLowerCase();
    };

    user.getBirthday = function () {
        let birthdayDay = this.birthday.substring(0, 2);
        let birthdayMonth = this.birthday.substring(3, 5);
        let birthdayYear = this.birthday.substring(6, 10);
        let birthdayData = new Date(birthdayYear, birthdayMonth - 1, birthdayDay).getFullYear();
        let currentData = new Date().getFullYear();
        let userAge = (currentData - birthdayData);
        return (new Date().getMonth() === birthdayMonth) ? ((new Date().getDay() > birthdayDay) ? userAge : (userAge - 1)) : ((new Date().getMonth() > birthdayMonth) ? userAge : (userAge - 1));
    };

    return user;
}

let user = createNewUser();
console.log(`User login is: ${user.getLogin()}`);
console.log(`User age is: ${user.getBirthday()} years`);