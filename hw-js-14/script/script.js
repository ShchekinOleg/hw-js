"use strict";

$(document).ready(function(){
    $("a[href*=#]").on("click", function(e){
        let anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 2000);
        e.preventDefault();
        return false;
    });
});

$(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        $('#button').addClass('show');
    } else {
        $('#button').removeClass('show');
    }
});

$('#button').on('click', function(e) {
    $('html, body').animate({scrollTop:0}, 5000);
});

$('#toggle').click(function () {
    $("#hide-section").slideToggle("slow");

});